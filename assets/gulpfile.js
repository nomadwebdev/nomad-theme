"use strict";

const gulp = require("gulp");
const babel = require("gulp-babel");
const sass = require("gulp-sass")(require("sass"));
const autoprefixer = require("gulp-autoprefixer");
const cleancss = require("gulp-clean-css");
const concat = require("gulp-concat");
const rename = require("gulp-rename");
const uglify = require("gulp-uglify");

/**
 * Here we set a prefix for our compiled and stylesheet and scripts.
 * Note that this should be the same as the `$themeHandlePrefix` in `func-script.php` and `func-style.php`.
 */
const themePrefix = "nomad-theme";

/**
 * Paths and files
 */

// Set path to Foundation files
const FOUNDATION = "node_modules/foundation-sites";

const srcScss = "scss/**/*.scss";
const srcJsDir = "js";
const srcJsFiles = [
  // Lets grab what-input first
  // 'node_modules/what-input/dist/what-input.js',
  "node_modules/slick-carousel/slick/slick.js",

  // Foundation core - needed if you want to use any of the components below
  FOUNDATION + "/dist/js/plugins/foundation.core.js",
  FOUNDATION + "/dist/js/plugins/foundation.util.*.js",

  // Pick the components you need in your project
  FOUNDATION + "/dist/js/plugins/foundation.abide.js",
  FOUNDATION + "/dist/js/plugins/foundation.accordion.js",
  FOUNDATION + "/dist/js/plugins/foundation.accordionMenu.js",
  FOUNDATION + "/dist/js/plugins/foundation.drilldown.js",
  FOUNDATION + "/dist/js/plugins/foundation.dropdown.js",
  FOUNDATION + "/dist/js/plugins/foundation.dropdownMenu.js",
  FOUNDATION + "/dist/js/plugins/foundation.equalizer.js",
  FOUNDATION + "/dist/js/plugins/foundation.interchange.js",
  FOUNDATION + "/dist/js/plugins/foundation.offcanvas.js",
  FOUNDATION + "/dist/js/plugins/foundation.orbit.js",
  FOUNDATION + "/dist/js/plugins/foundation.responsiveMenu.js",
  FOUNDATION + "/dist/js/plugins/foundation.responsiveToggle.js",
  FOUNDATION + "/dist/js/plugins/foundation.reveal.js",
  FOUNDATION + "/dist/js/plugins/foundation.slider.js",
  FOUNDATION + "/dist/js/plugins/foundation.smoothScroll.js",
  FOUNDATION + "/dist/js/plugins/foundation.magellan.js",
  FOUNDATION + "/dist/js/plugins/foundation.sticky.js",
  FOUNDATION + "/dist/js/plugins/foundation.tabs.js",
  FOUNDATION + "/dist/js/plugins/foundation.responsiveAccordionTabs.js",
  FOUNDATION + "/dist/js/plugins/foundation.toggler.js",
  FOUNDATION + "/dist/js/plugins/foundation.tooltip.js",

  // Place custom JS here, files will be concantonated, minified if ran with --production
  "js/scripts/**/*.js",
];
const destCss = "css";
const destJs = "js";

/**
 * Task for styles.
 *
 * Scss files are compiled and sent over to `assets/css/`.
 */
gulp.task("css", () => {
  return gulp
    .src(srcScss)
    .pipe(sass().on("error", sass.logError))
    .pipe(autoprefixer({ cascade: false }))
    .pipe(rename(`${themePrefix}.min.css`))
    .pipe(cleancss())
    .pipe(gulp.dest(destCss));
});

/**
 * Task for scripts.
 *
 * Js files are uglified and sent over to `assets/js/scripts/`.
 */
gulp.task("js", () => {
  return gulp
    .src(srcJsFiles)
    .pipe(
      babel({
        presets: ["es2015"],
      })
    )
    .pipe(concat(`${themePrefix}.min.js`))
    .pipe(uglify())
    .pipe(gulp.dest(destJs));
});

/**
 * Task for watching styles and scripts.
 */
gulp.task("watch", () => {
  gulp.watch(srcScss, gulp.series("css"));
  gulp.watch(srcJsFiles, gulp.series("js"));
});

/**
 * Default task
 */
gulp.task("default", gulp.series("css", "js"));
